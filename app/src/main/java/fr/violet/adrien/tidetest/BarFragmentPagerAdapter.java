package fr.violet.adrien.tidetest;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

public class BarFragmentPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;
    private Fragment[] registeredFragments = new Fragment[2];

    public BarFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return registeredFragments.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return BarListFragment.newInstance();
            case 1:
                return BarMapFragment.newInstance();
        }

        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments[position] = fragment;
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments[position] = null;
        super.destroyItem(container, position, object);
    }

    public Fragment getFragment(int position) {
        return registeredFragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.list_title);
            case 1:
                return mContext.getString(R.string.map_title);
        }

        return null;
    }
}