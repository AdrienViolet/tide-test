package fr.violet.adrien.tidetest.api.components;

import javax.inject.Singleton;

import dagger.Component;
import fr.violet.adrien.tidetest.BarActivity;
import fr.violet.adrien.tidetest.api.modules.NetModule;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 *
 */
@Singleton
@Component(modules = NetModule.class)
public interface NetComponent {

    OkHttpClient okHttpClient();

    Retrofit retrofit();

    void inject(BarActivity dependencyReceiver);
}
