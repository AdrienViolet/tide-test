package fr.violet.adrien.tidetest;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fr.violet.adrien.tidetest.models.Bar;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BarMapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BarMapFragment extends Fragment implements OnMapReadyCallback {

    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;

    private List<Bar> mBars;
    private List<Marker> mMarkers;

    public BarMapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BarMapFragment.
     */
    public static BarMapFragment newInstance() {
        BarMapFragment fragment = new BarMapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bar_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mBars != null) {
            addMarkers();
        }

        // we already have get the permission
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mGoogleMap.setMyLocationEnabled(true);
    }

    public void setData(List<Bar> bars) {
        this.mBars = bars;
        if (mGoogleMap != null) {
            addMarkers();
        }
    }

    private void addMarkers() {
        mGoogleMap.clear();

        if (mBars != null) {

            mMarkers = new ArrayList<>();

            // Create LatLngBound to get the right position and zoom for the gmap camera
            LatLngBounds.Builder bounds = LatLngBounds.builder();

            for (Bar bar :
                    mBars) {

                LatLng barLocation = new LatLng(bar.getGeometry().getLocation().getLat(),
                        bar.getGeometry().getLocation().getLng());

                bounds.include(barLocation);

                Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(barLocation)
                        .title(bar.getName())
                        .snippet(String.format(Locale.getDefault(), getActivity().getString(R.string.distance_placeholder), bar.getCurrentDistance())));

                marker.setTag(bar.getId());

                mMarkers.add(marker);
            }

            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 50));
        }
    }

    /**
     * Zoom on a bar and show the info window.
     * @param bar
     */
    public void selectBar(Bar bar) {
        for (Marker marker :
                mMarkers) {

            if (marker.getTag().equals(bar.getId())) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 20f));
                marker.showInfoWindow();
            }
        }
    }
}
