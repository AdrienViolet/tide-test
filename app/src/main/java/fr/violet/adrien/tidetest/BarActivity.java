package fr.violet.adrien.tidetest;

import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.Locale;

import javax.inject.Inject;

import fr.violet.adrien.tidetest.api.GPlacesAPIInterface;
import fr.violet.adrien.tidetest.models.Bar;
import fr.violet.adrien.tidetest.models.PlacesResponse;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BarActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        BarListFragment.OnBarInteractionListener {

    private static final int REQUEST_LOCATION = 1;

    @Inject
    GPlacesAPIInterface mApi;

    private ViewPager mViewPager;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private BarFragmentPagerAdapter mPagerAdapter;
    private boolean isWaitingForPermission = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar);

        ((TideTestApplication) getApplication())
                .getNetComponent().inject(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new BarFragmentPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mPagerAdapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        refreshData();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                isWaitingForPermission = false;
                refreshData();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.permission_title)
                        .setMessage(R.string.permission_message)
                        .setPositiveButton(R.string.ask_again, (dialog1, which) -> {
                            isWaitingForPermission = false;
                            refreshData();
                        })
                        .setNegativeButton(R.string.no, (dialog12, which) -> {
                            isWaitingForPermission = false;
                            handleError();
                        }).create().show();
            }
        }
    }

    /**
     * The method will notify the fragments the Activity is fetching the data,
     * check the permissions for the location, request them if necessary,
     * fetch the data from the Google Places API and populate the fragments.
     */
    private void refreshData() {

        if (mGoogleApiClient.isConnecting()) {
            return;
        }

        ((BarListFragment) mPagerAdapter.getFragment(0)).setLoading(true);

        // Check and request location permission if necessary
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (isWaitingForPermission) {
                return;
            }

            isWaitingForPermission = true;
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
        }

        // Get the latest position
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation != null) {

            // Fetch bars nearby
            Observable<PlacesResponse> mGPlacesObservable = mApi.getNearbyPlaces(String.format(Locale.getDefault(), getString(R.string.coord_placeholder), String.valueOf(mLocation.getLatitude()), String.valueOf(mLocation.getLongitude())), getString(R.string.rank_by_attribute),
                    getString(R.string.gplaces_bar_type),
                    getString(R.string.google_maps_key));

            mGPlacesObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<PlacesResponse>() {
                        @Override
                        public final void onCompleted() {
                            // do nothing
                        }

                        @Override
                        public final void onError(Throwable e) {
                            e.printStackTrace();
                            handleError();
                        }

                        @Override
                        public final void onNext(PlacesResponse response) {
                            if (response != null) {
                                if (response.getStatus().equals("OK")) {
                                    populateResults(response);
                                } else {
                                    handleError();
                                }
                            } else {
                                handleError();
                            }
                        }
                    });
        } else {
            handleError();
        }

    }

    /**
     * Populate the fragments with the answer from google places.
     *
     * @param response
     */
    private void populateResults(PlacesResponse response) {

        // Create a temporary Location to calculate the distance between
        // the current location of the user and the bar
        for (Bar bar :
                response.getBars()) {
            Location temp = new Location(LocationManager.GPS_PROVIDER);
            temp.setLatitude(bar.getGeometry().getLocation().getLat());
            temp.setLongitude(bar.getGeometry().getLocation().getLng());

            bar.setCurrentDistance(Math.round(mLocation.distanceTo(temp)));
        }

        ((BarListFragment) mPagerAdapter.getFragment(0)).setData(response.getBars());
        ((BarMapFragment) mPagerAdapter.getFragment(1)).setData(response.getBars());
    }

    /**
     * Empty the fragments and display an error message in a SnackBar.
     */
    private void handleError() {
        ((BarListFragment) mPagerAdapter.getFragment(0)).setData(null);
        ((BarMapFragment) mPagerAdapter.getFragment(1)).setData(null);

        Snackbar.make(mViewPager, R.string.error_message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onBarClicked(Bar bar) {
        mViewPager.setCurrentItem(1, true);
        ((BarMapFragment) mPagerAdapter.getFragment(1)).selectBar(bar);
    }

    @Override
    public void onRefreshRequested() {
        refreshData();
    }
}
