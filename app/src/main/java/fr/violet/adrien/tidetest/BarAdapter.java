package fr.violet.adrien.tidetest;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fr.violet.adrien.tidetest.models.Bar;

/**
 * Created by adrienviolet on 01/03/2017.
 */

public class BarAdapter extends RecyclerView.Adapter {

    private final Context mContext;
    private List<Bar> mData = new ArrayList<>();

    public BarAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BarViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_bar, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Bar item = mData.get(position);

        ((BarViewHolder) holder).mName.setText(item.getName());
        ((BarViewHolder) holder).mDistance.setText(String.format(Locale.getDefault(), mContext.getString(R.string.distance_placeholder), item.getCurrentDistance()));

        ColorDrawable placeholderDrawable = new ColorDrawable(mContext.getResources().getColor(R.color.primary));

        if (item.getPhotos() != null && item.getPhotos().size() > 0) {
            String photoUrl = String.format(Locale.getDefault(), mContext.getString(R.string.google_places_photo_url), item.getPhotos().get(0).getPhotoReference(), mContext.getString(R.string.google_maps_key));
            Picasso.with(mContext).load(photoUrl)
                    .placeholder(placeholderDrawable)
                    .error(placeholderDrawable)
                    .into(((BarViewHolder) holder).mPhoto);
        } else {
            ((BarViewHolder) holder).mPhoto.setImageDrawable(placeholderDrawable);
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<Bar> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public Bar getItem(int position) {
        return mData.get(position);
    }

    private static class BarViewHolder extends RecyclerView.ViewHolder {

        private final TextView mName;
        private final TextView mDistance;
        private final ImageView mPhoto;

        public BarViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.name);
            mDistance = (TextView) view.findViewById(R.id.distance);
            mPhoto = (ImageView) view.findViewById(R.id.photo);
        }
    }
}
