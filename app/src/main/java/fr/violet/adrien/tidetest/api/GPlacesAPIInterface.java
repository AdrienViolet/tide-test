package fr.violet.adrien.tidetest.api;

import fr.violet.adrien.tidetest.models.PlacesResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 *
 */

public interface GPlacesAPIInterface {

    @GET("place/nearbysearch/json")
    Observable<PlacesResponse> getNearbyPlaces(@Query("location") String location,
                                               @Query("rankby") String rankby,
                                               @Query("type") String type,
                                               @Query("key") String apiKey);

}