package fr.violet.adrien.tidetest;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import fr.violet.adrien.tidetest.api.components.DaggerNetComponent;
import fr.violet.adrien.tidetest.api.components.NetComponent;
import fr.violet.adrien.tidetest.api.modules.NetModule;

/**
 * Created by adrienviolet on 01/03/2017.
 */

public class TideTestApplication extends MultiDexApplication {

    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Init net component for API calls
        mNetComponent =
                DaggerNetComponent.builder()
                        .netModule(new NetModule(getString(R.string.google_places_base_url), this))
                        .build();

    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
