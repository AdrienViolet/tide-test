package fr.violet.adrien.tidetest.api.modules;

import android.content.Context;

import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.violet.adrien.tidetest.api.GPlacesAPIInterface;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Provide an instance of the http client and an instance of GPlacesAPI using this same http client
 */
@Module
public class NetModule {

    String mBaseUrl;
    Context mContext;

    // Constructor needs one parameter to instantiate.
    public NetModule(String baseUrl, Context context) {
        this.mBaseUrl = baseUrl;
        this.mContext = context;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient().newBuilder();

        OkHttpClient okClient = okHttpClientBuilder.build();

        return okClient;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {

        GsonBuilder builder = new GsonBuilder();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(builder.create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public GPlacesAPIInterface providesGPlacesApi(Retrofit retrofit) {
        return retrofit.create(GPlacesAPIInterface.class);
    }

}
