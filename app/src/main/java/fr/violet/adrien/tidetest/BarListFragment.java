package fr.violet.adrien.tidetest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fr.violet.adrien.tidetest.models.Bar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BarListFragment.OnBarInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BarListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BarListFragment extends Fragment {

    private OnBarInteractionListener mListener;
    private BarAdapter mBarAdapter;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mRefreshLayout;

    public BarListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BarListFragment.
     */
    public static BarListFragment newInstance() {
        BarListFragment fragment = new BarListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBarAdapter = new BarAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bar_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(() -> {
            if (mListener != null) {
                mListener.onRefreshRequested();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.nb_column)));
        mRecyclerView.setAdapter(mBarAdapter);

        // Setup an ItemTouchListener here instead of handling click in the Adapter
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                (view1, position) -> {
                    Bar item = mBarAdapter.getItem(position);
                    if (mListener != null) {
                        mListener.onBarClicked(item);
                    }
                }));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBarInteractionListener) {
            mListener = (OnBarInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnBarInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setLoading(boolean isLoading) {
        mRefreshLayout.setRefreshing(isLoading);
    }

    public void setData(List<Bar> bars) {
        setLoading(false);
        if (bars == null || bars.size() == 0) {
            mBarAdapter.setData(new ArrayList<>());
        } else {
            mBarAdapter.setData(bars);
        }

    }

    public interface OnBarInteractionListener {
        void onBarClicked(Bar bar);

        void onRefreshRequested();
    }
}
